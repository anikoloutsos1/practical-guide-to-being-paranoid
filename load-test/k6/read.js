import http from 'k6/http';
import {sleep, check} from 'k6';

export const options = {
    thresholds: {
        // 90% of requests must finish within 400ms, 95% within 600, and 99.9% within 2s.
        http_req_duration: ['p(90) < 400', 'p(95) < 600', 'p(99.9) < 2000'],
    },
};


export default function () {
    const accessToken = getAccessToken()
    const response = http.get(
        'http://localhost:8080/api/v1/patterns',
        {
            headers: {
                authorization: 'Bearer ' + accessToken
            }
        }
    )
    check(response, {'Patterns responded with status 200': (r) => r.status === 200})
    // Meetup - Do we need to verify the body of the response here?
    sleep(1);
}

export function getAccessToken() {
    const a = http.post(
        'http://localhost:8081/realms/patterns/protocol/openid-connect/token',
        {
            grant_type: 'password',
            username: 'potato',
            password: 'potato',
            client_id: 'the-frontend',
            headers: {
                'Content-type': 'application/x-www-form-urlencoded'
            }
        }
    )
    return JSON.parse(a.body)['access_token']
}
