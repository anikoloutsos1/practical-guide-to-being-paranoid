package nl.gerimedica.architecture.patterns.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.web.SecurityFilterChain;



@Configuration
public class SecurityConfig {

    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        // @formatter:off
        http
                .antMatcher("/**")
                    .authorizeRequests()
                .mvcMatchers("/actuator/**")
                .permitAll()
                        .anyRequest()
                            .authenticated()
                .and()
                    .oauth2ResourceServer(OAuth2ResourceServerConfigurer::jwt)
                        .cors();
        return http.build();
        // @formatter:on
    }
}

