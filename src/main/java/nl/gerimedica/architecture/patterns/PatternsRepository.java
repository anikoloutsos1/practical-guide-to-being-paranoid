package nl.gerimedica.architecture.patterns;

import nl.gerimedica.architecture.patterns.model.Pattern;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatternsRepository extends JpaRepository<Pattern, String> {
    boolean existsByName(String name);
}
