package nl.gerimedica.architecture.patterns;

import nl.gerimedica.architecture.patterns.exception.PatternAlreadyExists;
import nl.gerimedica.architecture.patterns.model.Pattern;
import nl.gerimedica.architecture.patterns.model.PatternDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatternsService {

    private final PatternsRepository patternsRepository;

    public PatternsService(PatternsRepository patternsRepository) {
        this.patternsRepository = patternsRepository;
    }

    public List<PatternDTO> getAll() {
        return patternsRepository.findAll()
                .stream()
                .map(PatternDTO::fromPatternEntity)
                .toList();
    }
    public PatternDTO savePattern(PatternDTO patternDTO) throws PatternAlreadyExists {
        if (patternsRepository.existsByName(patternDTO.name()))
            throw new PatternAlreadyExists(String.format("pattern with name: %s already exists.",  patternDTO.name()));
        return PatternDTO.fromPatternEntity(patternsRepository.save(Pattern.fromDTO(patternDTO)));
    }
}
