package nl.gerimedica.architecture.patterns;

import nl.gerimedica.architecture.patterns.exception.PatternAlreadyExists;
import nl.gerimedica.architecture.patterns.model.PatternDTO;
import nl.gerimedica.architecture.patterns.model.Response;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/patterns")
public class PatternsController {

    private final PatternsService patternsService;

    public PatternsController(PatternsService patternsService) {
        this.patternsService = patternsService;
    }

    @GetMapping()
    Response<List<PatternDTO>> getAll() {
        return new Response<>(patternsService.getAll());
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    Response<PatternDTO> createPattern(@RequestBody @Valid PatternDTO patternDTO) throws PatternAlreadyExists {
        return new Response<>(patternsService.savePattern(patternDTO));
    }


}
