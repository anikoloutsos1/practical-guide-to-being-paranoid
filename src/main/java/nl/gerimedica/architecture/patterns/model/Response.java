package nl.gerimedica.architecture.patterns.model;

public record Response<L>(L data) { }
