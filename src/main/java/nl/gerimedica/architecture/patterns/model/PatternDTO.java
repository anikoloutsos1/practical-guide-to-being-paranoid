package nl.gerimedica.architecture.patterns.model;

import javax.validation.constraints.Size;

public record PatternDTO(String id, @Size(max = 128) String subClassification, @Size(max = 255)  String name, @Size(max = 10_000)  String description){

    public static PatternDTO fromPatternEntity(Pattern p) {
        return new PatternDTO(p.getId().toString(), p.getSubClassification(), p.getName(), p.getDescription());
    }
}
