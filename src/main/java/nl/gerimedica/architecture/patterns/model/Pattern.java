package nl.gerimedica.architecture.patterns.model;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;
@Entity
public class Pattern {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;
    @Column(length = 128)
    private String subClassification;
    private String name;
    @Column(length = 10_000)
    private String description;

    public Pattern(UUID id, String subClassification, String name, String description) {
        this.id = id;
        this.subClassification = subClassification;
        this.name = name;
        this.description = description;
    }

    public Pattern() {
        this(null, null, null, null);
    }


    public static Pattern fromDTO(PatternDTO dto) {
        return new Pattern(dto.id() == null ? null : UUID.fromString(dto.id()), dto.subClassification(), dto.name(), dto.description());
    }

    public UUID getId() {
        return id;
    }

    @SuppressWarnings("unused")
    public void setId(UUID id) {
        this.id = id;
    }

    public String getSubClassification() {
        return subClassification;
    }

    @SuppressWarnings("unused")
    public void setSubClassification(String subClassification) {
        this.subClassification = subClassification;
    }

    public String getName() {
        return name;
    }

    @SuppressWarnings("unused")
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    @SuppressWarnings("unused")
    public void setDescription(String description) {
        this.description = description;
    }
}
