package nl.gerimedica.architecture.patterns.exception;


public class PatternAlreadyExists extends Exception {
    public PatternAlreadyExists(String msg) {
        super(msg);
    }
}
