package nl.gerimedica.architecture.patterns.util;

import dasniko.testcontainers.keycloak.KeycloakContainer;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.PostgreSQLContainer;

public class Initializer {
    private static final String POSTGRES_VERSION = "14.5";

    public static PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer("postgres:" + POSTGRES_VERSION)
            .withDatabaseName("integration-tests-db")
            .withUsername("sa")
            .withPassword("sa");

    public static class PostgresInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            postgreSQLContainer.start();
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }

    private static final String KEYCLOAK_VERSION = "19.0.0";
    private static final String KEYCLOAK_DOCKER_IMAGE_LOCATION = "quay.io/keycloak/keycloak:";
    public static final String PATTERNS_KEYCLOAK_REALM = "patterns";

    static final KeycloakContainer keycloak = new KeycloakContainer(KEYCLOAK_DOCKER_IMAGE_LOCATION + KEYCLOAK_VERSION)
            .withRealmImportFile("keycloak/realm-export.json");

    public static class KeycloakInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            keycloak.start();
            TestPropertyValues.of("spring.security.oauth2.resourceserver.jwt.issuer-uri=" +
                            "http://localhost:" + getKeycloakPort() + "/realms/" + PATTERNS_KEYCLOAK_REALM)
                    .applyTo(configurableApplicationContext.getEnvironment());
        }
    }

    public static int getKeycloakPort() {
        return keycloak.getHttpPort();
    }

}
