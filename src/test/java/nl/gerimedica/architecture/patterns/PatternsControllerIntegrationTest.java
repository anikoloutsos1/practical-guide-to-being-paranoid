package nl.gerimedica.architecture.patterns;

import nl.gerimedica.architecture.patterns.model.PatternDTO;
import nl.gerimedica.architecture.patterns.util.Initializer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.UUID;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.matchesRegex;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * The security is disabled by default for the tests. {@link PatternsControllerAuthTest} is used for authentication related tests
 * In this test we are testing the interaction between controller-service-repo-database
  */
@SpringBootTest
@ContextConfiguration(initializers = { Initializer.PostgresInitializer.class})
class PatternsControllerIntegrationTest {


    private static final String PATTERN_BASE_URL = "/api/v1/patterns";
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private PatternsRepository patternsRepository;

    MockMvc mockMvc;

    public static final String UUID_V4_STRING =
            "[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[89abAB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}";

    @BeforeEach
    void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        patternsRepository.deleteAll();
    }

    @Test
    void shouldReturnEmptyListOfPatterns_WhenRequestingPatternsBeforeAddingAny() throws Exception {

        var response = """
                { \
                  "data": [] \
                } \
                """.replaceAll("\\s", "");
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(PATTERN_BASE_URL)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(response));
    }

    private static Stream<PatternDTO> validPattern() {
        return Stream.of(
                new PatternDTO(
                        null,
                        "creational",
                        "Dependency Injection",
                        "A class accepts the objects it requires from an injector instead of creating the objects directly."
                ),
                new PatternDTO(
                        UUID.randomUUID().toString(),
                        "Max Name",
                        "Max Name Size is 255 characters".repeat(10).substring(0,255),
                        "Max Name"
                ),
                new PatternDTO(
                        UUID.randomUUID().toString(),
                        "Max Sub classification size is 128 characters".repeat(4).substring(0,128),
                        "Max Sub classification",
                        "Max Sub classification"
                ),new PatternDTO(
                        UUID.randomUUID().toString(),
                        "Max description",
                        "Max description",
                        "Max description size is 10000 characters ".repeat(300).substring(0,10_000)
                )
        );
    }

    @ParameterizedTest
    @MethodSource("validPattern")
    void shouldAddOnePattern_WhenPostingWithValidBody(PatternDTO patternDTO) throws Exception {
        var requestBody = String.format("""
                { \
                    "subClassification": "%s", \
                    "name": "%s", \
                    "description": "%s" \
                    } \
                """, patternDTO.subClassification(), patternDTO.name(), patternDTO.description());
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(PATTERN_BASE_URL)
                                .content(requestBody)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isCreated())
                .andDo(print())
                .andExpect(jsonPath("$.data.subClassification", is(patternDTO.subClassification())))
                .andExpect(jsonPath("$.data.name", is(patternDTO.name())))
                .andExpect(jsonPath("$.data.description", is(patternDTO.description())))
                .andExpect(jsonPath("$.data.id", matchesRegex(UUID_V4_STRING)))
                .andExpect(jsonPath("$.data.id", not(patternDTO.id())));
    }

    @Test
    void shouldThrowBadRequest_GivenRequestIsInvalid() throws Exception {
        var subClassification = "Max Sub classification size is 128 characters".repeat(3).substring(0,129);
        var name = "Max Name Size is 255 characters".repeat(10).substring(0,256);
        var description = "Max description size is 10000 characters ".repeat(300).substring(0,10_001);
        var requestBody = String.format("""
                { \
                    "subClassification": "%s", \
                    "name": "%s", \
                    "description": "%s" \
                    } \
                """, subClassification, name, description);

        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(PATTERN_BASE_URL)
                                .content(requestBody)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isBadRequest());

        mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(PATTERN_BASE_URL)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.length()", is(0)));
    }

    @Test
    void shouldWriteData_GivenSqlInjectionAttempt() throws Exception {
        var subClassification = "'A;DROP TABLE pattern'";
        var requestBody = String.format("""
                { \
                    "subClassification": "%s", \
                    "name": "the name", \
                    "description": "the description" \
                    } \
                """, subClassification);

        mockMvc.perform(
                        MockMvcRequestBuilders
                                .post(PATTERN_BASE_URL)
                                .content(requestBody)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isCreated());

        mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(PATTERN_BASE_URL)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.length()", is(1)));
    }
}
