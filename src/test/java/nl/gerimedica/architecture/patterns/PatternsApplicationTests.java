package nl.gerimedica.architecture.patterns;

import nl.gerimedica.architecture.patterns.util.Initializer;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(initializers = {Initializer.PostgresInitializer.class})
class PatternsApplicationTests {

	@SuppressWarnings("java:S2699")
	@Test
	void contextLoads() {
	}

}
