package nl.gerimedica.architecture.patterns;

import nl.gerimedica.architecture.patterns.exception.PatternAlreadyExists;
import nl.gerimedica.architecture.patterns.model.PatternDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

/**
 * There is no good reason to expand the service tests to every scenario that is covered already by the controllerIntegrationTest!
 */
@ExtendWith(MockitoExtension.class)
class PatternsServiceTest {

    PatternsService patternsService;

    @Mock
    PatternsRepository patternsRepository;

    @BeforeEach
    void setup() {
        patternsService = new PatternsService(patternsRepository);
    }

    @Test
    void shouldThrowException_GivenPatternWithTheSameNameExists() {
        var name = "the name";
        when(patternsRepository.existsByName(name)).thenReturn(true);
        assertThrows(PatternAlreadyExists.class, () ->
                patternsService.savePattern(new PatternDTO(null, null, name, null))
        );

    }

}
