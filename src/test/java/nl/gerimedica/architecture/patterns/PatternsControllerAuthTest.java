package nl.gerimedica.architecture.patterns;

import nl.gerimedica.architecture.patterns.util.Initializer;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static nl.gerimedica.architecture.patterns.util.Initializer.PATTERNS_KEYCLOAK_REALM;
import static nl.gerimedica.architecture.patterns.util.Initializer.getKeycloakPort;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * The Auth test initializes a keycloak instance and verifies that only authorized access is granted to the Patterns endpoints
 */
@WebMvcTest(PatternsController.class)
@ContextConfiguration(initializers = { Initializer.KeycloakInitializer.class })
class PatternsControllerAuthTest {

    private static final String PATTERN_BASE_URL = "/api/v1/patterns";
    @Autowired
    MockMvc mockMvc;
    @MockBean
    PatternsService patternsService;

    @Test
    void shouldReturnUnauthorized_GivenNoTokenIsProvided() throws Exception {
        when(patternsService.getAll()).thenReturn(List.of());
        var response = "";
        mockMvc.perform(MockMvcRequestBuilders
                        .get(PATTERN_BASE_URL))
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andExpect(content().string(response));
    }

    @ParameterizedTest
    @ValueSource(strings = {"Bearer", "Bearer POTATO", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"})
    void shouldReturnUnauthorized_GivenInvalidToken(String invalidBearerToken) throws Exception {

        var response = "";
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(PATTERN_BASE_URL)
                                .header(HttpHeaders.AUTHORIZATION, invalidBearerToken)
                )
                .andExpect(status().isUnauthorized())
                .andExpect(content().string(response));
    }

    @Test
    void shouldReturnEmptyListOfPatterns_WhenRequestingPatternsBeforeAddingAny_WithAValidToken() throws Exception {

        var response = """
                { \
                  "data": [] \
                } \
                """.replaceAll("\\s", "");
        var accessToken = loginToKeycloakThroughTheFrontEndAndGetAccessToken();
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .get(PATTERN_BASE_URL)
                                .header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(response));
    }

    @NotNull
    private static String loginToKeycloakThroughTheFrontEndAndGetAccessToken() {
        var restTemplate = new RestTemplate();
        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.put("grant_type", List.of("password"));
        body.put("client_id", List.of("the-frontend"));
        body.put("username", List.of("potato"));
        body.put("password", List.of("potato"));
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        var keycloakLoginUrl = "http://localhost:" + getKeycloakPort() + "/realms/" + PATTERNS_KEYCLOAK_REALM + "/protocol/openid-connect/token";
        var httpEntity = new HttpEntity<>(body, headers);
        ResponseEntity<String> stringResponseEntity = restTemplate.postForEntity(keycloakLoginUrl, httpEntity, String.class);
        JacksonJsonParser jsonParser = new JacksonJsonParser();
        return jsonParser.parseMap(stringResponseEntity.getBody())
                .get("access_token")
                .toString();
    }

}
